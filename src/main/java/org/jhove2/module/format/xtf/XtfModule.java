/**
 * 
 */
package org.jhove2.module.format.xtf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jhove2.annotation.ReportableProperty;
import org.jhove2.core.JHOVE2;
import org.jhove2.core.JHOVE2Exception;
import org.jhove2.core.format.Format;
import org.jhove2.core.io.Input;
import org.jhove2.core.source.Source;
import org.jhove2.module.format.BaseFormatModule;
import org.jhove2.module.format.Validator;
import org.jhove2.persist.FormatModuleAccessor;

import ch.htwchur.xtfvt.*;
import ch.htwchur.xtfvt.datastructures.*;

import com.beust.jcommander.JCommander;
import com.sleepycat.persist.model.Persistent;

/**
 * @author ltog
 *
 */
@Persistent
public class XtfModule
	extends BaseFormatModule
	implements Validator
{
	/** XTF module version identifier */
	public static final String VERSION = "0.0.1";
	
	/** XTF module release date */
	public static final String RELEASE = "2014-04-23";
	
	/** XTF module rights statement */	
	public static final String RIGHTS = "Unlicensed by Lukas Toggenburger (see http://unlicense.org/)";
	
	/** XTF module validation coverage */
	public static final Coverage COVERAGE = Coverage.Selective;
	
	/** validation status */
	protected Validity validity = Validity.Undetermined;
	
	/** error messages created by xtfvt*/
	protected List<String> xtfvtErrorMessages = new ArrayList<String>();
	
	@SuppressWarnings("unused")
	private XtfModule() {
		this(null, null);
	}

	/**
	 * XTF format module constructor
	 * @param format Format object with information, including format aliases, for the format or format family which this module is constructed to parse
	 * @param formatModuleAccessor Utility object that manages persistence for format modules
	 */
	public XtfModule(Format format, FormatModuleAccessor formatModuleAccessor) {
		super(format, formatModuleAccessor);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param version
	 * @param release
	 * @param rights
	 * @param format
	 * @param formatModuleAccessor
	 */
	public XtfModule(String version, String release, String rights,
			Format format, FormatModuleAccessor formatModuleAccessor)
	{
		super(VERSION, RELEASE, RIGHTS, format, formatModuleAccessor);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param version
	 * @param release
	 * @param rights
	 * @param scope
	 * @param format
	 * @param formatModuleAccessor
	 */
	public XtfModule(String version, String release, String rights, Scope scope, Format format,
			FormatModuleAccessor formatModuleAccessor) {
		super(VERSION, RELEASE, RIGHTS, scope, format, formatModuleAccessor);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public long parse(JHOVE2 jhove2, Source source, Input input)
	{
		this.validity = Validity.Undetermined;
	
		String[] args = { "-x", source.getFile().getPath() };

		Settings settings = new Settings();
		new JCommander(settings, args);
		
		Xtfvt xtfvt = new Xtfvt(settings);
		
		XtfValidationResultWithErrorList resultWithErrors = null;
		try {
			resultWithErrors = xtfvt.validate();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.xtfvtErrorMessages = resultWithErrors.getErrors();
		this.validity = convertValidationResult(resultWithErrors.getXtfValidationResult());
		
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.jhove2.module.format.Validator#validate(org.jhove2.core.JHOVE2, org.jhove2.core.source.Source, org.jhove2.core.io.Input)
	 */
	@Override
	public Validity validate(JHOVE2 jhove2, Source source, Input input) throws JHOVE2Exception {
		// in extreme cases, this method may be implemented in a way such that it returns
		// simply the validation status without doing any validation at all (validation
		// is then done in parse() )
		return this.validity;
	}

	/**
	 * Get module validation coverage.
	 * 
	 * @return the coverage
	 */
	@Override
	public Coverage getCoverage() {
		return COVERAGE;
	}

	/** Get source unit's XML validation status.
	 * 
	 * @return the validity
	 */
	@Override
	public Validity isValid() {
		return this.validity;
	}
	
	/**
	 * 
	 */
	@ReportableProperty(order = 1, value = "List of xtfvt error messages")
	public List<String> getXtfvtErrorMessages() {
		return this.xtfvtErrorMessages;
	}


	/**
	 * Convert from xtfvt's XtfValidationResult to JHOVE2's Validity 
	 * 
	 * @param xtfValidationResult xtfvt's result of the validation
	 * @return Validity compatible with JHOVE2
	 */
	private Validity convertValidationResult(XtfValidationResult xtfValidationResult) {
		switch(xtfValidationResult) {
		case PASS:
			return Validity.True;
		case FAIL:
			return Validity.False;
		case UNDECIDED:
			return Validity.Undetermined;
		default:
			return Validity.Undetermined;
		}
	}
}
