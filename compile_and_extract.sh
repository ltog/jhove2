#!/bin/bash

mvn -DskipTests=true install

echo ""
echo "Going to extract jhove2-2.1.0.zip ..."

cd target/
unzip -q -o jhove2-2.1.0.zip && echo -e "\nFinished extracting jhove2-2.1.0.zip ...\n"
cd ..

cp -v ../xtfvt/target/xtfvt-0.0.1-SNAPSHOT.jar target/jhove2-2.1.0/lib/
cp -v ../xtfvt/src/main/resources/ili2c.jar    target/jhove2-2.1.0/lib/
