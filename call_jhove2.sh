#!/bin/bash

./target/jhove2-2.1.0/jhove2.sh "$@" | fgrep -v '[main] WARN  TypeConverterDelegate : PropertyEditor [com.sun.beans.editors.EnumEditor] found through deprecated global PropertyEditorManager fallback - consider using a more isolated form of registration, e.g. on the BeanWrapper/BeanFactory!'
